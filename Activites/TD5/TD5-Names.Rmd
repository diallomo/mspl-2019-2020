---
title: "French given names per year per department"
author: "Lucas Mello Schnorr, Jean-Marc Vincent"
date: "February, 2018"
output:
  pdf_document: default
  html_document:
    df_print: paged
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Using the [given names data set of INSEE](https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_txt.zip), answer some of the following questions:

- Choose a first name (yours) analyse the evolution of its frequency along time?
- What can we say about ``Your name here'' (for each state, all the country)?
- Is there some sort of geographical correlation with the data? (optional)
- Which department has a larger variety of names along time? (optional)
- _your own question_ (be creative, not optional)

You need to use the _tidyverse_ for this analysis. Unzip the file _dpt2018_txt.zip_ (to get the **dpt2018.txt**). Read in R with this code. Note that you might need to install the `readr` package with the appropriate command.

## Download Raw Data from the website
```{r}
file = "dpt2018_txt.zip"
if(!file.exists(file)){
  download.file("https://www.insee.fr/fr/statistiques/fichier/2540004/dpt2018_csv.zip",
	destfile=file)
}
unzip(file)

```

## Build the Dataframe from file

```{r}
library(tidyverse)
df <- read_delim("dpt2018.csv",delim=";");
head(df)
```

Test et analyse selon le prenom

```{r}
df %>%
  filter(preusuel == 'MOHAMED') %>%
  mutate(annee = as.integer(annais)) %>%
  filter(annee > 1998) %>%
  filter(dpt == 38) %>%
  ggplot() +
    aes(x = annee) +
    aes(y = nombre) +
    geom_line()
```


Prenom le plus frequent par annee et par sexe

```{r}
df %>%
  filter(annais != 'XXXX') %>%


```

